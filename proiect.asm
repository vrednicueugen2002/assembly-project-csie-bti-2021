.model small
.stack 100h
.data
mesInitial db "Introduceti un sir de caractere: $"
mesFinal db "Sirul contine atatea litere: $"
buton db 13
litereCount dw 0
contor db 0
aux db ?
.code
start:
mov ax, @data
mov ds, ax

lea dx, mesInitial
mov ah, 9h
int 21h
 
	repetare:

mov ah, 1h
int 21h

cmp  al, buton
je sfarsit

cmp al, 5Ah
jg intreLitere

cmp al, 40h
jg plusLitera

jmp repetare

intreLitere:
cmp al, 61h
jl repetare

cmp al, 7Bh
jl plusLitera

jmp repetare

plusLitera:
inc litereCount
jmp repetare
	sfarsit:

lea dx, mesFinal
mov ah, 9h
int 21h

	validare:
mov ax, litereCount
mov bl, 10
div bl
push ax
inc contor
mov ah, 0
mov literecount, ax

cmp litereCount, 0
jne validare

mov cx, 0
mov cl, contor

	afisare:
pop dx
mov aux, dh
mov dl, aux
add dl, 48
mov ah, 2h
int 21h
	loop afisare

mov ax, 4C00h
int 21h
end start
